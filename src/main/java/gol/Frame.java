package gol;

import java.util.Arrays;

public class Frame {

    private int width;
    private int height;
    private boolean[][] frameState;

    public Frame(int width, int height) {
        this.width = width;
        this.height = height;
        frameState = new boolean[height][width];
    }

    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < frameState.length; i++) {
            if (i != 0)
                b.append("\n");
            b.append(printOneDimensionalArray(frameState[i]));
        }
        return b.toString();
    }

    public String printOneDimensionalArray(boolean[] array) {
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < array.length; i++) {
            if (i != 0)
                b.append(" ");
            if (array[i])
                b.append("X");
            else
                b.append("-");
        }
        return b.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Frame))
            return false;
        Frame f = (Frame) obj;
        boolean equality = true;
        for (int i = 0; i < frameState.length; i++) {
            if (!Arrays.equals(frameState[i], f.frameState[i]))
                equality = false;
        }
        return equality;
    }

    public Integer getNeighbourCount(int x, int y) {
        Integer count = 0;
        for (int row = y - 1; row <= y + 1; row++) {
            for (int col = x - 1; col <= x + 1; col++) {
                if (!(row == y && col == x) && neighbourIsWithinTheBorders(row, col) && frameState[row][col]) {
                    count += 1;
                }
            }
        }
        return count;
    }

    public boolean neighbourIsWithinTheBorders(int row, int col) {
        return row >= 0 && row < height && col >= 0 && col < width;
    }

    public boolean isAlive(int x, int y) {
        return frameState[y][x];
    }

    public void markAlive(int x, int y) {
        frameState[y][x] = true;
    }

    public Frame nextFrame() {
        Frame nextFrame = new Frame(this.width, this.height);
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (isAlive(col, row) && (getNeighbourCount(col, row) == 2 || getNeighbourCount(col, row) == 3))
                    nextFrame.markAlive(col, row);
                else if (!isAlive(col, row) && getNeighbourCount(col, row) == 3)
                    nextFrame.markAlive(col, row);
            }
        }
        return nextFrame;
    }

}