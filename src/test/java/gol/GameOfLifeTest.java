package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class GameOfLifeTest {

    @Test
    public void framesWithDifferentSizeAreNotEqual() {
        Frame f1 = new Frame(1, 1);
        Frame f2 = new Frame(2, 2);
        assertThat(f1.equals(f2), is(false));
    }

    @Test
    public void framesWithTheSameSizeAndStateAreEqual() {
        Frame f1 = new Frame(3, 3);
        Frame f2 = new Frame(3, 3);
        f1.markAlive(1, 2);
        f2.markAlive(1, 2);
        f1.markAlive(0, 0);
        f2.markAlive(0, 0);
        assertThat(f1.equals(f2), is(true));
    }

    @Test
    public void everyObjectIsEqualToItself() {
        Frame f1 = new Frame(3, 3);
        Frame f2 = f1;
        assertThat(f1.equals(f2), is(true));
    }

    @Test
    public void noObjectIsEqualToNull() {
        Frame f1 = new Frame(3, 3);
        Frame f2 = null;
        assertThat(f1.equals(f2), is(false));
    }

    @Test
    public void deadCellsAreGivenVisualRepresentationOfMinusSign() {
        Frame f1 = new Frame(1, 1);
        assertThat(f1.toString(), is("-"));
    }

    @Test
    public void aliveCellsAreGivenVisualRepresentationOfX() {
        Frame f1 = new Frame(1, 1);
        f1.markAlive(0, 0);
        assertThat(f1.toString(), is("X"));
    }

    @Test
    public void cellsCanBeMarkedAsAlive() {
        Frame f = new Frame(2, 2);
        f.markAlive(1, 1);
        assertThat(f.isAlive(0, 0), is(false));
        assertThat(f.isAlive(0, 1), is(false));
        assertThat(f.isAlive(1, 0), is(false));
        assertThat(f.isAlive(1, 1), is(true));
    }

    @Test
    public void cellsNeighboursCanBeCountedAllAroundTheCell() {
        Frame f = new Frame(4, 4);
        f.markAlive(1, 1);
        f.markAlive(0, 0);
        f.markAlive(0, 1);
        f.markAlive(0, 2);
        f.markAlive(1, 0);
        f.markAlive(1, 2);
        f.markAlive(2, 0);
        f.markAlive(2, 1);
        f.markAlive(2, 2);
        f.markAlive(3, 3);
        assertThat(f.getNeighbourCount(1, 1), is(8));
    }

    @Test
    public void cellsNeighboursCanBeCountedInTheCornerOfTheFrame() {
        Frame f = new Frame(3, 3);
        f.markAlive(0, 0);
        f.markAlive(0, 1);
        f.markAlive(1, 0);
        f.markAlive(2, 0);
        assertThat(f.getNeighbourCount(0, 0), is(2));
    }

    @Test
    public void nextFrameHasTheSameNumberOfRowsAndColumnsAsThePreviousCell() {
        Frame f1 = new Frame(3, 5);
        Frame f2 = f1.nextFrame();
        assertThat(f2.equals(f1), is(true));
    }

    @Test
    public void cellsWithLessThanTwoNeighboursDie() {
        Frame frame = new Frame(3, 3);
        frame.markAlive(1, 1);
        frame.markAlive(0, 0);
        Frame nextFrame = frame.nextFrame();
        assertThat(nextFrame.isAlive(1, 1), is(false));
    }

    @Test
    public void cellsWithTwoOrThreeNeighboursLiveOn() {
        Frame frame = new Frame(3, 3);
        frame.markAlive(1, 1);
        frame.markAlive(0, 0);
        frame.markAlive(0, 1);
        Frame nextFrame = frame.nextFrame();
        assertThat(nextFrame.isAlive(1, 1), is(true));
    }

    @Test
    public void cellsWithMoreThanThreeNeighboursDie() {
        Frame frame = new Frame(3, 3);
        frame.markAlive(1, 1);
        frame.markAlive(0, 0);
        frame.markAlive(0, 2);
        frame.markAlive(2, 0);
        frame.markAlive(2, 2);
        Frame nextFrame = frame.nextFrame();
        assertThat(nextFrame.isAlive(1, 1), is(false));
    }

    @Test
    public void deadCellsWithThreeNeighboursBecomeAlive() {
        Frame frame = new Frame(3, 3);
        frame.markAlive(0, 0);
        frame.markAlive(0, 2);
        frame.markAlive(2, 0);
        Frame nextFrame = frame.nextFrame();
        assertThat(nextFrame.isAlive(1, 1), is(true));
    }

    @Test
    public void allTheRulesSuccessfullyWorkTogether() {
        Frame f1 = new Frame(5, 3);
        f1.markAlive(0, 0);
        f1.markAlive(1, 0);
        f1.markAlive(1, 1);
        f1.markAlive(2, 1);
        f1.markAlive(3, 1);
        f1.markAlive(4, 1);
        f1.markAlive(2, 2);
        Frame f2 = new Frame(5, 3);
        f2.markAlive(0, 0);
        f2.markAlive(1, 0);
        f2.markAlive(3, 0);
        f2.markAlive(0, 1);
        f2.markAlive(3, 1);
        f2.markAlive(1, 2);
        f2.markAlive(2, 2);
        assertThat(f1.nextFrame().equals(f2), is(true));
    }
}